NuGet Server
===================
A very simple NuGet server for my personal use, similar to [NuGet.Server](https://docs.microsoft.com/en-us/nuget/hosting-packages/nuget-server) but in Go language. Designed for scenarios where a single user (ie. a person or a build server) pushes packages.

Folder
===================
```
./
    --- nuget.server.exe
    --- packages/
           |-------- xxx.xxx.nupkg
```
