package main

import (
	"github.com/gorilla/mux"
	"net/http"
	"fmt"
	"flag"
	"log"
)

func main() {
	port := flag.String("port", "4000", "Port Number")
	base := flag.String("baseUrl", "http://localhost:4000/nuget/", "Base Url of nuget")
	flag.Parse()

	nuget := InitNuGet(*base)

	router := mux.NewRouter()
	router.PathPrefix("/packages/").Handler(http.StripPrefix("/packages/", http.FileServer(http.Dir("./packages"))))

	nuget_router := router.PathPrefix("/nuget").Subrouter()
	nuget_router.HandleFunc("", nuget.Welcome).Methods("GET")
	nuget_router.HandleFunc("/", nuget.Welcome).Methods("GET")
	nuget_router.HandleFunc("/$metadata", nuget.GetMetadata).Methods("GET")
	nuget_router.HandleFunc("/FindPackagesById()", nuget.Search).Methods("GET")
	nuget_router.HandleFunc("/Packages", nuget.Search).Methods("GET")
	nuget_router.HandleFunc("/Packages()", nuget.Search).Methods("GET")
	nuget_router.HandleFunc("/Packages(Id='{id}',Version='{version}')", nuget.GetPackageInfo).Methods("GET")
	nuget_router.HandleFunc("/package/{id}/{version}", nuget.Download).Methods("GET")

	log.Printf("Start HTTP Server on 0.0.0.0:%s", *port)
	err := http.ListenAndServe(fmt.Sprintf(":%s", *port), router)
	if err != nil {
		log.Fatal("cannot start web service: ", err)
	}
}
