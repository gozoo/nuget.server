package main

import (
	"net/http"
	"github.com/gorilla/mux"
	"fmt"
	"log"
	"path/filepath"
	"archive/zip"
	"strings"
	"encoding/xml"
	"io/ioutil"
	"os"
	"encoding/base64"
	"text/template"
	"unicode"
	"time"
	"crypto/sha512"
	"io"
)
type Dependencies struct {
	Dependency string
}
type Metadata struct {
	Id                string    `xml:"id"`
	Version           string    `xml:"version"`
	Title             string    `xml:"title"`
	Authors           string    `xml:"authors"`
	Owners            string    `xml:"owners"`
	ProjectUrl        string    `xml:"projectUrl"`
	IconUrl           string    `xml:"iconUrl"`
	LicenseUrl        string    `xml:"licenseUrl"`
	Description       string    `xml:"description"`
	ReleaseNotes      string    `xml:"releaseNotes"`
	Copyright         string    `xml:"copyright"`
	Summary           string    `xml:"summary,omitempty"`
	Dependencies      *Dependencies `xml:"dependencies,omitempty"`
	BaseUrl			  string
	PackageTime       string
	PackageSize       int64
	PackageHash       string
	NormalizedVersion string
	IsPrerelease      bool
}

type Package struct {
	XMLName  xml.Name     `xml:"http://schemas.microsoft.com/packaging/2013/05/nuspec.xsd package"`
	Metadata *Metadata    `xml:"metadata"`
}

type NuGet struct {
	BaseUrl string
	PackageInfo map[string]*Metadata
}

func (dep *Dependencies) UnmarshalXML(d *xml.Decoder, start xml.StartElement) error {
	deps := make([]string, 0)
	for {
		t, err := d.Token()
		if err == io.EOF { // found end of element
			break
		}
		if err != nil {
			return err
		}
		if el, ok := t.(xml.StartElement); ok {
			if el.Name.Local == "dependency" {
				ns := make(map[string]string)
				for _, attr := range el.Attr {
					if attr.Name.Local == "id" {
						ns["id"] = attr.Value
					} else if attr.Name.Local == "version" {
						ns["version"] = attr.Value
					}
				}
				deps = append(deps, fmt.Sprintf("%s:%s", ns["id"], ns["version"]))
			}
		}
	}
	dep.Dependency = strings.Join(deps, "|")
	return nil
}

func InitNuGet(baseUrl string) *NuGet {
	//currentDir, err := os.Executable()
	//if err != nil {
	//	log.Panic(err)
	//}
	//packageDir := filepath.Join(filepath.Dir(currentDir), "packages", "*.nupkg")
	packageDir := filepath.Join("packages", "*.nupkg")
	files, err := filepath.Glob(packageDir)
	if err != nil {
		log.Panic(err)
	}
	nuget := &NuGet{}
	if strings.HasSuffix(baseUrl, "/") {
		nuget.BaseUrl = baseUrl
	} else {
		nuget.BaseUrl = fmt.Sprintf("%s/", baseUrl)
	}
	for _, pkg := range files {
		nuget.parsePackage(pkg)
	}
	return nuget
}

func (nuget *NuGet) parsePackage(pkg string) {
	if nuget.PackageInfo == nil {
		nuget.PackageInfo = make(map[string]*Metadata)
	}
	_, name := filepath.Split(pkg)
	metadata := parseSpecFile(pkg)
	if metadata == nil {
		return
	}
	tm, size, hash := calcNupkg(pkg)
	metadata.PackageSize = size
	metadata.PackageHash = hash
	metadata.PackageTime = tm.UTC().Format(time.RFC3339)
	metadata.BaseUrl = nuget.BaseUrl
	version, prelease := parseVersion(metadata.Version)
	metadata.NormalizedVersion = version
	metadata.IsPrerelease = prelease
	nuget.PackageInfo[name] = metadata
}

func parseSpecFile(pkg string) *Metadata {
	nupkg, err := zip.OpenReader(pkg)
	if err != nil {
		log.Panic(err)
	}
	defer nupkg.Close()
	for _, file := range nupkg.File {
		if strings.HasSuffix(file.Name, ".nuspec") {
			reader, _ := file.Open()
			data, _ := ioutil.ReadAll(reader)
			reader.Close()
			var p Package
			err := xml.Unmarshal(data, &p)
			if err != nil {
				log.Panic(err)
			}
			if p.Metadata.Dependencies == nil {
				p.Metadata.Dependencies = &Dependencies{""}
			}
			return p.Metadata
		}
	}
	return nil
}

func calcNupkg(pkg string) (time.Time, int64, string) {
	info, _ := os.Stat(pkg)
	tm := info.ModTime()
	hasher := sha512.New()
	nupkg, err := os.Open(pkg)
	if err != nil {
		log.Panic(err)
	}
	defer nupkg.Close()
	size, err := io.Copy(hasher, nupkg)
	hash := hasher.Sum(nil)
	return tm, size, base64.StdEncoding.EncodeToString(hash)
}

func parseVersion(version string) (string, bool) {
	ver := make([]rune, 0)
	for _, c := range version {
		if unicode.IsDigit(c) || c == '.' {
			ver = append(ver, c)
		} else {
			break
		}
	}
	ret := string(ver)
	if ret == version {
		return version, false
	}
	return strings.Join(strings.Split(ret, "."), "."), true
}

func (nuget *NuGet) Welcome(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/xml; charset=utf-8")
	w.Write([]byte(welcome_banner))
}

func (nuget *NuGet) GetMetadata(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/xml; charset=utf-8")
	w.Write([]byte(metadata))
}
func (nuget *NuGet) Search(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/atom+xml; charset=utf-8")
	data := make(map[string]interface{})
	data["BaseUrl"] = nuget.BaseUrl
	data["Updated"] = time.Now().UTC().Format(time.RFC3339)
	data["Title"] = "Packages"
	entries := make([]*Metadata,len(nuget.PackageInfo))
	var idx = 0
	for _, v := range nuget.PackageInfo {
		entries[idx] = v
		idx ++
	}
	data["Entries"] = entries
	temp := template.Must(template.New("Search").Parse(feed_template + entry_template))
	err := temp.Execute(w, data)
	if err != nil {
		log.Panic(err)
	}
}
func (nuget *NuGet) GetPackageInfo(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["id"]
	version := vars["version"]
	pkgName := fmt.Sprintf("%s.%s.nupkg", id, version)
	metadata, ok := nuget.PackageInfo[pkgName]
	if ok {
		w.Header().Set("Content-Type", "application/atom+xml; charset=utf-8")
		temp := template.Must(template.New("Search").Parse(package_template + entry_template))
		err := temp.Execute(w, metadata)
		if err != nil {
			log.Panic(err)
		}
	} else {
		http.Error(w, fmt.Sprintf("%s: Not Found", pkgName), 404)
	}
}

func (nuget *NuGet) Download(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["id"]
	version := vars["version"]
	pkgUrl := fmt.Sprintf("/packages/%s.%s.nupkg", id, version)
	http.Redirect(w, r, pkgUrl, 301)
}

const welcome_banner = `<?xml version="1.0" encoding="utf-8"?>
<service xmlns="http://www.w3.org/2007/app" xmlns:atom="http://www.w3.org/2005/Atom">
  <workspace>
    <atom:title>Default</atom:title>
    <collection href="Packages">
      <atom:title>Packages</atom:title>
    </collection>
  </workspace>
</service>`

const metadata = `<edmx:Edmx xmlns:edmx="http://schemas.microsoft.com/ado/2007/06/edmx" Version="1.0">
<edmx:DataServices xmlns:m="http://schemas.microsoft.com/ado/2007/08/dataservices/metadata" m:DataServiceVersion="2.0">
<Schema xmlns:d="http://schemas.microsoft.com/ado/2007/08/dataservices" xmlns:m="http://schemas.microsoft.com/ado/2007/08/dataservices/metadata" xmlns="http://schemas.microsoft.com/ado/2006/04/edm" Namespace="NuGet.Server.DataServices">
<EntityType Name="ODataPackage" m:HasStream="true">
<Key>
<PropertyRef Name="Id"/>
<PropertyRef Name="Version"/>
</Key>
<Property Name="Id" Type="Edm.String" Nullable="false" m:FC_TargetPath="SyndicationTitle" m:FC_ContentKind="text" m:FC_KeepInContent="false"/>
<Property Name="Version" Type="Edm.String" Nullable="false"/>
<Property Name="NormalizedVersion" Type="Edm.String" Nullable="true"/>
<Property Name="IsPrerelease" Type="Edm.Boolean" Nullable="false"/>
<Property Name="Title" Type="Edm.String" Nullable="true"/>
<Property Name="Authors" Type="Edm.String" Nullable="true" m:FC_TargetPath="SyndicationAuthorName" m:FC_ContentKind="text" m:FC_KeepInContent="false"/>
<Property Name="Owners" Type="Edm.String" Nullable="true"/>
<Property Name="IconUrl" Type="Edm.String" Nullable="true"/>
<Property Name="LicenseUrl" Type="Edm.String" Nullable="true"/>
<Property Name="ProjectUrl" Type="Edm.String" Nullable="true"/>
<Property Name="DownloadCount" Type="Edm.Int32" Nullable="false"/>
<Property Name="RequireLicenseAcceptance" Type="Edm.Boolean" Nullable="false"/>
<Property Name="DevelopmentDependency" Type="Edm.Boolean" Nullable="false"/>
<Property Name="Description" Type="Edm.String" Nullable="true"/>
<Property Name="Summary" Type="Edm.String" Nullable="true" m:FC_TargetPath="SyndicationSummary" m:FC_ContentKind="text" m:FC_KeepInContent="false"/>
<Property Name="ReleaseNotes" Type="Edm.String" Nullable="true"/>
<Property Name="Published" Type="Edm.DateTime" Nullable="false"/>
<Property Name="LastUpdated" Type="Edm.DateTime" Nullable="false" m:FC_TargetPath="SyndicationUpdated" m:FC_ContentKind="text" m:FC_KeepInContent="false"/>
<Property Name="Dependencies" Type="Edm.String" Nullable="true"/>
<Property Name="PackageHash" Type="Edm.String" Nullable="true"/>
<Property Name="PackageHashAlgorithm" Type="Edm.String" Nullable="true"/>
<Property Name="PackageSize" Type="Edm.Int64" Nullable="false"/>
<Property Name="Copyright" Type="Edm.String" Nullable="true"/>
<Property Name="Tags" Type="Edm.String" Nullable="true"/>
<Property Name="IsAbsoluteLatestVersion" Type="Edm.Boolean" Nullable="false"/>
<Property Name="IsLatestVersion" Type="Edm.Boolean" Nullable="false"/>
<Property Name="Listed" Type="Edm.Boolean" Nullable="false"/>
<Property Name="VersionDownloadCount" Type="Edm.Int32" Nullable="false"/>
<Property Name="MinClientVersion" Type="Edm.String" Nullable="true"/>
<Property Name="Language" Type="Edm.String" Nullable="true"/>
</EntityType>
<EntityContainer Name="PackageContext" m:IsDefaultEntityContainer="true">
<EntitySet Name="Packages" EntityType="NuGet.Server.DataServices.ODataPackage"/>
<FunctionImport Name="Search" EntitySet="Packages" ReturnType="Collection(NuGet.Server.DataServices.ODataPackage)" m:HttpMethod="GET">
<Parameter Name="searchTerm" Type="Edm.String" Mode="In"/>
<Parameter Name="targetFramework" Type="Edm.String" Mode="In"/>
<Parameter Name="includePrerelease" Type="Edm.Boolean" Mode="In"/>
<Parameter Name="includeDelisted" Type="Edm.Boolean" Mode="In"/>
</FunctionImport>
<FunctionImport Name="FindPackagesById" EntitySet="Packages" ReturnType="Collection(NuGet.Server.DataServices.ODataPackage)" m:HttpMethod="GET">
<Parameter Name="id" Type="Edm.String" Mode="In"/>
</FunctionImport>
<FunctionImport Name="GetUpdates" EntitySet="Packages" ReturnType="Collection(NuGet.Server.DataServices.ODataPackage)" m:HttpMethod="GET">
<Parameter Name="packageIds" Type="Edm.String" Mode="In"/>
<Parameter Name="versions" Type="Edm.String" Mode="In"/>
<Parameter Name="includePrerelease" Type="Edm.Boolean" Mode="In"/>
<Parameter Name="includeAllVersions" Type="Edm.Boolean" Mode="In"/>
<Parameter Name="targetFrameworks" Type="Edm.String" Mode="In"/>
<Parameter Name="versionConstraints" Type="Edm.String" Mode="In"/>
</FunctionImport>
</EntityContainer>
</Schema>
</edmx:DataServices>
</edmx:Edmx>
`

const feed_template = `<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<feed xml:base="{{.BaseUrl}}" xmlns="http://www.w3.org/2005/Atom" xmlns:d="http://schemas.microsoft.com/ado/2007/08/dataservices" xmlns:m="http://schemas.microsoft.com/ado/2007/08/dataservices/metadata">
  <id>{{.BaseUrl}}{{.Title}}</id>
  <title type="text">{{.Title}}</title>
  <updated>{{.Updated}}</updated>
  <link rel="self" title="{{.Title}}" href="{{.Title}}" />
  {{range .Entries -}}{{template "entry" .}}{{- end}}
</feed>
`
const package_template = `<?xml version="1.0" encoding="utf-8"?>
{{template "entry" .}}
`

const entry_template = `{{define "entry" -}}
<entry>
  <id>{{.BaseUrl}}Packages(Id='{{.Id}}',Version='{{.Version}}')</id>
  <title type="text">{{.Id}}</title>
  <summary type="text">{{.Summary}}</summary>
  <updated>{{.PackageTime}}</updated>
  <author>
    <name>{{.Authors}}</name>
  </author>
  <link rel="edit-media" title="ODataPackage" href="Packages(Id='{{.Id}}',Version='{{.Version}}')/$value" />
  <link rel="edit" title="ODataPackage" href="Packages(Id='{{.Id}}',Version='{{.Version}}')" />
  <category term="NuGet.Server.DataServices.ODataPackage" scheme="http://schemas.microsoft.com/ado/2007/08/dataservices/scheme" />
  <content type="application/zip" src="{{.BaseUrl}}package/{{.Id}}/{{.Version}}" />
  <m:properties xmlns:m="http://schemas.microsoft.com/ado/2007/08/dataservices/metadata" xmlns:d="http://schemas.microsoft.com/ado/2007/08/dataservices">
    <d:Version>{{.Version}}</d:Version>
    <d:NormalizedVersion>{{.NormalizedVersion}}</d:NormalizedVersion>
    <d:IsPrerelease m:type="Edm.Boolean">{{.IsPrerelease}}</d:IsPrerelease>
    <d:Title>{{.Title}}</d:Title>
    <d:Owners>{{.Owners}}</d:Owners>
    <d:IconUrl>{{.IconUrl}}</d:IconUrl>
    <d:LicenseUrl>{{.LicenseUrl}}</d:LicenseUrl>
    <d:ProjectUrl>{{.ProjectUrl}}</d:ProjectUrl>
    <d:DownloadCount m:type="Edm.Int32">-1</d:DownloadCount>
    <d:RequireLicenseAcceptance m:type="Edm.Boolean">false</d:RequireLicenseAcceptance>
    <d:DevelopmentDependency m:type="Edm.Boolean">false</d:DevelopmentDependency>
    <d:Description>{{.Description}}</d:Description>
    <d:ReleaseNotes>{{.ReleaseNotes}}</d:ReleaseNotes>
    <d:Published m:type="Edm.DateTime">{{.PackageTime}}</d:Published>
    <d:Dependencies>{{.Dependencies.Dependency}}</d:Dependencies>
    <d:PackageHash>{{.PackageHash}}</d:PackageHash>
    <d:PackageHashAlgorithm>SHA512</d:PackageHashAlgorithm>
    <d:PackageSize m:type="Edm.Int64">{{.PackageSize}}</d:PackageSize>
    <d:Copyright>{{.Copyright}}</d:Copyright>
    <d:Tags m:null="true"></d:Tags>
    <d:IsAbsoluteLatestVersion m:type="Edm.Boolean">true</d:IsAbsoluteLatestVersion>
    <d:IsLatestVersion m:type="Edm.Boolean">true</d:IsLatestVersion>
    <d:Listed m:type="Edm.Boolean">true</d:Listed>
    <d:VersionDownloadCount m:type="Edm.Int32">-1</d:VersionDownloadCount>
    <d:MinClientVersion m:null="true"></d:MinClientVersion>
    <d:Language m:null="true"></d:Language>
    <d:Summary m:null="true"></d:Summary>
  </m:properties>
</entry>
{{- end}}
`
